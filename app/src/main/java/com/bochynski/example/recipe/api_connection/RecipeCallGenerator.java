package com.bochynski.example.recipe.api_connection;

import com.bochynski.example.recipe.model.response_brief.ResponseBrief;
import com.bochynski.example.recipe.model.response_recipe_detailed.ResponseRecipeDetails;

import retrofit2.Call;

/**
 * Created by Karlo on 2017-08-24.
 */

public class RecipeCallGenerator {

    private static RecipeApiService apiService;

    static {
        apiService = RecipeRetroClient.getApiService();
    }

    private RecipeCallGenerator() {

    }

    public static Call<ResponseBrief> getCall(String appID, String apiKey, String search, String maxTimeSeconds, String[] dietsAllowed, String[] allergiesAllowed, String courseAllowed) {
        return apiService.getRecipes(appID, apiKey, search, maxTimeSeconds, dietsAllowed, allergiesAllowed, courseAllowed);
    }

    public static Call<ResponseRecipeDetails> getCall(String recipeID, String appID, String apiKey) {
        return apiService.getRecipeInfo(recipeID, appID, apiKey);
    }
}
