
package com.bochynski.example.recipe.model.response_brief;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Criteria {

    @SerializedName("q")
    @Expose
    private String query;
    @SerializedName("allowedIngredient")
    @Expose
    private Object allowedIngredient;
    @SerializedName("excludedIngredient")
    @Expose
    private Object excludedIngredient;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Object getAllowedIngredient() {
        return allowedIngredient;
    }

    public void setAllowedIngredient(Object allowedIngredient) {
        this.allowedIngredient = allowedIngredient;
    }

    public Object getExcludedIngredient() {
        return excludedIngredient;
    }

    public void setExcludedIngredient(Object excludedIngredient) {
        this.excludedIngredient = excludedIngredient;
    }

}
