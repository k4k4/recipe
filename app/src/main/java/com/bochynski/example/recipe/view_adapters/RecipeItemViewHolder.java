package com.bochynski.example.recipe.view_adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bochynski.example.recipe.R;
import com.squareup.picasso.Picasso;

import java.net.URL;

/**
 * Created by Karlo on 2017-08-24.
 */

public class RecipeItemViewHolder extends RecyclerView.ViewHolder {

    private CardView recipeCard;
    private ImageView recipeImage;
    private TextView recipeTitle;
    private TextView recipeScore;
    private TextView recipePreparationTime;

    public RecipeItemViewHolder(View view) {
        super(view);
        recipeCard = (CardView) view.findViewById(R.id.recipe_item_card);
        recipeImage = (ImageView) view.findViewById(R.id.recipe_image);
        recipeTitle = (TextView) view.findViewById(R.id.recipe_title);
        recipeScore = (TextView) view.findViewById(R.id.recipe_rating);
        recipePreparationTime = (TextView) view.findViewById(R.id.recipe_prep_time);
    }

    public void setRecipeImage(Context context, String imageURL) {
        Picasso.with(context).load(imageURL).centerCrop().fit().into(recipeImage);
    }

    public void setRecipeTitle(String text) {
        recipeTitle.setText(text);
    }

    public void setRecipeScore(Context context, int score) {
        recipeScore.setText(score + " " + context.getString(R.string.star_symbol));
    }

    @SuppressWarnings("SpellCheckingInspection")
    public void setRecipePreparationTime(Context context, int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        if (hours > 0) {
            recipePreparationTime.setText(String.format("%s: %sh %smin", context.getString(R.string.time), hours, minutes));
        } else {
            recipePreparationTime.setText(String.format("%s: %smin", context.getString(R.string.time), minutes));
        }
    }

    public View getViewFrame() {
        return recipeCard;
    }
}
