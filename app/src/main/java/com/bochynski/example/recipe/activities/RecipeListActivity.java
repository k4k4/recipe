package com.bochynski.example.recipe.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.api_connection.RecipeCallGenerator;
import com.bochynski.example.recipe.api_connection.RecipeMetaDataCallGenerator;
import com.bochynski.example.recipe.model.meta_data.MetaDataCourse;
import com.bochynski.example.recipe.model.response_brief.MatchRecipe;
import com.bochynski.example.recipe.model.response_brief.ResponseBrief;
import com.bochynski.example.recipe.utils.StorageFavoritesHandler;
import com.bochynski.example.recipe.view_adapters.NavigationItemArrayAdapter;
import com.bochynski.example.recipe.view_adapters.RecipeItemRecyclerViewAdapter;
import com.bochynski.example.recipe.views_utils.NavigationItem;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity representing a list of Recipes. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link RecipeDetailsActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class RecipeListActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_SETTINGS = 99;
    public static final String API_KEY = "8e9473b3dc09a07b73ad099a05a1e239";
    public static final String APP_ID = "2446dedb";
    public static final String SEARCH_KEY_DIETS = "diets";
    public static final String SEARCH_KEY_ALLERGIES = "allergies";
    public static final String SEARCH_KEY_MAX_TIME = "max time";

    private static final String SAVED_MAX_TIME = "Saved max time";
    private static final String SAVED_DIETS = "Saved diets";
    private static final String SAVED_ALLERGIES = "Saved allergies";
    private static final String LOG_TAG = RecipeListActivity.class.getSimpleName();
    private static final String SAVED_USER_QUERY = "Saved query";
    private static final String SAVED_USER_COURSE = "Saved course";
    private static final String SAVED_COURSE_SEARCH_VALUE = "Saved course search value";
    private static String[] coursesPredefined;
    private static TypedArray iconsCoursesPredefined;

    private boolean isTwoPane;
    private boolean navigationReady;
    private List<MatchRecipe> recipes;
    private NavigationItemArrayAdapter navigationAdapter;
    private List<NavigationItem> navigationItems;
    private String courseSearchValue, courseSelection;
    private String userQuery;
    private NavigationItem favorites, searchAll;
    private ActionBarDrawerToggle drawerToggle;
    private String activityTitle;
    private BroadcastReceiver broadcastReceiver;
    private Snackbar snackbar;
    private int maxTimeSeconds = Integer.MAX_VALUE;
    private String[] allergies;
    private String[] diets;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_list)
    ListView navigationList;

    @BindView(R.id.recipe_list)
    RecyclerView recyclerView;

    @BindView(R.id.search_summary)
    TextView searchSummary;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    @BindView(R.id.loading_info)
    LinearLayout loadingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipes_main);
        ButterKnife.bind(this);
        setUpLoading();
        restoreData(savedInstanceState);
        validateSearchSummary();
        navigationReady = false;
        coursesPredefined = getResources().getStringArray(R.array.courses_predefined);
        iconsCoursesPredefined = getResources().obtainTypedArray(R.array.courses_predefined_icons);

        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        activityTitle = getTitle().toString();
        setUpDrawer();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        assert recyclerView != null;

        if (hasNetworkConnection()) {
            generateNavigationItems();
            makeApiCall();
        } else {
            showSnackbarDismissOnly(R.string.offline_info_snackbar, R.string.got_it);
        }

        if (findViewById(R.id.recipe_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp). then 2 pane mode
            isTwoPane = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initReceiverAndFilter();
    }

    private void restoreData(Bundle savedState) {
        if (savedState != null) {
            courseSelection = savedState.getString(SAVED_USER_COURSE);
            courseSearchValue = savedState.getString(SAVED_COURSE_SEARCH_VALUE);
            userQuery = savedState.getString(SAVED_USER_QUERY);
            maxTimeSeconds = savedState.getInt(SAVED_MAX_TIME);
            allergies = savedState.getStringArray(SAVED_ALLERGIES);
            diets = savedState.getStringArray(SAVED_DIETS);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVED_USER_COURSE, courseSelection);
        outState.putString(SAVED_USER_QUERY, userQuery);
        outState.putString(SAVED_COURSE_SEARCH_VALUE, courseSearchValue);
        outState.putInt(SAVED_MAX_TIME, maxTimeSeconds);
        outState.putStringArray(SAVED_DIETS, diets);
        outState.putStringArray(SAVED_ALLERGIES, allergies);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                userQuery = query;
                validateSearchSummary();
                searchView.setIconified(true);
                searchView.clearFocus();
                if (menu != null) {
                    menu.findItem(R.id.app_bar_search).collapseActionView();
                }

                if (hasNetworkConnection()) {
                    makeApiCall();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userQuery = "";
                validateSearchSummary();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.app_bar_reload:
                if (hasNetworkConnection()) {
                    generateNavigationItems();
                    makeApiCall();
                } else if (snackbar != null && !snackbar.isShown()) {
                    showSnackbarDismissOnly(R.string.offline_info_snackbar, R.string.got_it);
                }
                return true;
            case R.id.app_bar_settings:
                Intent i = new Intent(this, UserSettings.class);
                startActivityForResult(i, REQUEST_CODE_SETTINGS);
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new RecipeItemRecyclerViewAdapter(this, isTwoPane, recipes));
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setUpDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_search);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(activityTitle);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void makeApiCall() {
        Call<ResponseBrief> call = generateCall();
        call.enqueue(new Callback<ResponseBrief>() {
            @Override
            public void onResponse(Call<ResponseBrief> call, Response<ResponseBrief> response) {
                Log.v(LOG_TAG, "API response: " + response.code());
                Log.v(LOG_TAG, "API call: " + call.request().toString());
                if (response.isSuccessful()) {
                    ResponseBrief body = response.body();
                    recipes = body.getMatches();
                    setupRecyclerView(recyclerView);

                    Integer matchCount = body.getTotalMatchCount();
                    if (matchCount == 0) {
                        Toast.makeText(RecipeListActivity.this, R.string.no_recipes_found, Toast.LENGTH_SHORT).show();
                    }
                    setUpViewsData();
                } else {
                    Log.e(LOG_TAG, "Failure. Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBrief> call, Throwable t) {
                Log.e(LOG_TAG, "Response failed");
            }
        });
    }

    private Call<ResponseBrief> generateCall() {
        return RecipeCallGenerator.getCall(APP_ID, API_KEY, userQuery, Integer.toString(maxTimeSeconds), diets, allergies, courseSearchValue);
    }

    private void generateNavigationItems() {
        if (navigationReady) {
            return;
        }
        final Call<List<MetaDataCourse>> meta = RecipeMetaDataCallGenerator.getCallCourses(APP_ID, API_KEY);
        navigationItems = new LinkedList<>();
        favorites = new NavigationItem("Favorites", R.mipmap.ic_favorites, "");
        searchAll = new NavigationItem("Search all", R.mipmap.ic_chef, "");
        navigationItems.add(searchAll);
        navigationItems.add(favorites);
        meta.enqueue(new Callback<List<MetaDataCourse>>() {
            @Override
            public void onResponse(Call<List<MetaDataCourse>> call, Response<List<MetaDataCourse>> response) {
                Log.v(LOG_TAG, "OnResponse meta " + call.request().url());
                if (response.isSuccessful()) {
                    List<MetaDataCourse> metaDatas = response.body();
                    for (MetaDataCourse md : metaDatas) {
                        String name = md.getName();
                        int iconResource = matchIconIfEquipped(name);
                        NavigationItem navItem = new NavigationItem(md.getName(), iconResource, md.getSearchValue());
                        navigationItems.add(navItem);
                    }
                    initNavigationAdapterAndList();
                    navigationReady = true;
                } else {
                    Log.e(LOG_TAG, "response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<MetaDataCourse>> call, Throwable t) {
                Log.e(LOG_TAG, "failure, course meta data. " + t.getMessage());
            }
        });
    }

    private void initNavigationAdapterAndList() {
        navigationAdapter = new NavigationItemArrayAdapter(RecipeListActivity.this, R.layout.navigation_item, navigationItems);
        navigationList.setAdapter(navigationAdapter);
        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NavigationItem item = navigationItems.get(position);
                boolean handled = false;
                if (item.getText().equals(favorites.getText())) {
                    searchSummary.setVisibility(View.GONE);
                    courseSelection = "";
                    courseSearchValue = "";
                    userQuery = "";
                    displayFavorites();
                    handled = true;
                } else if (item.getText().equals(searchAll.getText())) {
                    courseSelection = "";
                    courseSearchValue = "";
                    validateSearchSummary();
                } else {
                    courseSearchValue = item.getSearchValue();
                    courseSelection = item.getText();
                    validateSearchSummary();
                }
                if (!handled && hasNetworkConnection()) {
                    makeApiCall();
                }
            }
        });
    }

    private void displayFavorites() {
        List<MatchRecipe> favorites = StorageFavoritesHandler.getFavorites(RecipeListActivity.this);
        if (favorites != null && favorites.size() > 0) {
            recipes = favorites;
            setupRecyclerView(recyclerView);
            setUpViewsData();
        } else {
            Toast.makeText(this, R.string.no_items_favorites, Toast.LENGTH_SHORT).show();
        }
    }

    private int matchIconIfEquipped(String courseName) {
        int iconResource;
        int indexOfItem = Arrays.asList(coursesPredefined).indexOf(courseName);
        if (indexOfItem >= 0 && indexOfItem < iconsCoursesPredefined.length()) {
            iconResource = iconsCoursesPredefined.getResourceId(indexOfItem, R.mipmap.ic_default_food);
        } else {
            iconResource = R.mipmap.ic_default_food;
        }
        return iconResource;
    }

    private void validateSearchSummary() {
        StringBuilder builder = new StringBuilder();
        if (!TextUtils.isEmpty(courseSelection)) {
            builder.append("#" + courseSelection);
        }
        if (!TextUtils.isEmpty(userQuery)) {
            builder.append(" #" + userQuery);
        }
        if (maxTimeSeconds != Integer.MAX_VALUE) {
            int hours = maxTimeSeconds / 3600;
            int minutes = (maxTimeSeconds % 3600) / 60;
            if (hours > 0) {
                builder.append(" #time:" + hours + "h " + minutes + "min");
            } else {
                builder.append(" #time: " + minutes + "min");
            }
        }
        if (allergies != null && allergies.length > 0) {
            builder.append(" #allergies");
        }
        if (diets != null && diets.length > 0) {
            builder.append(" #diets");
        }
        String result = builder.toString();
        if (!TextUtils.isEmpty(result)) {
            searchSummary.setVisibility(View.VISIBLE);
            searchSummary.setText(result);
        } else {
            searchSummary.setVisibility(View.GONE);
        }
    }

    private void initReceiverAndFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                    NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                    if (info.isConnected() || hasNetworkConnection()) {
                        dismissSnackbar();
                    } else if (!hasNetworkConnection()) {
                        showSnackbarDismissOnly(R.string.offline_info_snackbar, R.string.got_it);
                    }
                }
            }
        };
        registerReceiver(broadcastReceiver, filter);
    }

    private void showSnackbarDismissOnly(int resourceMessage, int resourceAction) {
        snackbar = Snackbar.make(drawerLayout, resourceMessage, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(resourceAction, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    private void dismissSnackbar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    private void setUpLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
    }

    private void setUpViewsData() {
        loadingLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    private boolean hasNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SETTINGS) {
            if (resultCode == UserSettings.RESULT_OK) {
                maxTimeSeconds = data.getIntExtra(SEARCH_KEY_MAX_TIME, Integer.MAX_VALUE);
                if (maxTimeSeconds != Integer.MAX_VALUE) {
                    maxTimeSeconds = maxTimeSeconds * 60;
                }
                diets = data.getStringArrayExtra(SEARCH_KEY_DIETS);
                allergies = data.getStringArrayExtra(SEARCH_KEY_ALLERGIES);
                validateSearchSummary();
                if (hasNetworkConnection()) {
                    makeApiCall();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }
        super.onPause();
    }
}
