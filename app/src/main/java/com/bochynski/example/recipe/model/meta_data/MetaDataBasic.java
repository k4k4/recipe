package com.bochynski.example.recipe.model.meta_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Karlo on 2017-08-26.
 */

public class MetaDataBasic {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("searchValue")
    @Expose
    private String searchValue;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("localesAvailableIn")
    @Expose
    private List<String> localesAvailableIn = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getLocalesAvailableIn() {
        return localesAvailableIn;
    }

    public void setLocalesAvailableIn(List<String> localesAvailableIn) {
        this.localesAvailableIn = localesAvailableIn;
    }
}
