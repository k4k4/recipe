
package com.bochynski.example.recipe.model.response_brief;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseBrief {

    @SerializedName("criteria")
    @Expose
    private Criteria criteria;
    @SerializedName("matches")
    @Expose
    private List<MatchRecipe> matches = null;
    @SerializedName("totalMatchCount")
    @Expose
    private Integer totalMatchCount;
    @SerializedName("attribution")
    @Expose
    private Attribution attribution;

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public List<MatchRecipe> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchRecipe> matches) {
        this.matches = matches;
    }

    public Integer getTotalMatchCount() {
        return totalMatchCount;
    }

    public void setTotalMatchCount(Integer totalMatchCount) {
        this.totalMatchCount = totalMatchCount;
    }

    public Attribution getAttribution() {
        return attribution;
    }

    public void setAttribution(Attribution attribution) {
        this.attribution = attribution;
    }

}
