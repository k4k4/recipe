
package com.bochynski.example.recipe.model.response_recipe_detailed;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecipeAttributes {

    @SerializedName("cuisine")
    @Expose
    private List<String> cuisine = null;
    @SerializedName("course")
    @Expose
    private List<String> course = null;

    public List<String> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<String> cuisine) {
        this.cuisine = cuisine;
    }

    public List<String> getCourse() {
        return course;
    }

    public void setCourse(List<String> course) {
        this.course = course;
    }

}
