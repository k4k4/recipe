package com.bochynski.example.recipe.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.api_connection.RecipeCallGenerator;
import com.bochynski.example.recipe.fragments.AboutFragment;
import com.bochynski.example.recipe.model.response_brief.ImageUrlsBySize;
import com.bochynski.example.recipe.model.response_brief.MatchRecipe;
import com.bochynski.example.recipe.model.response_recipe_detailed.Flavors;
import com.bochynski.example.recipe.model.response_recipe_detailed.ResponseRecipeDetails;
import com.bochynski.example.recipe.view_adapters.SectionsPagerAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bochynski.example.recipe.utils.SharedKeys.*;

public class RecipeDetailsActivity extends AppCompatActivity {

    public static final String SEARCH_ID_KEY = "Search id key";
    private static final String LOG_TAG = RecipeDetailsActivity.class.getSimpleName();
    private String recipeID;
    private ResponseRecipeDetails recipeDetails;
    private Bundle bundleStart;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);

        handleIntentAndCallApi(getIntent());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void handleIntentAndCallApi(Intent intent) {
        if (intent != null) {
            Bundle data = intent.getBundleExtra(KEY_BUNDLE_MATCH_RECIPE);
            if (data != null) {
                recipeID = data.getString(KEY_ID);
                bundleStart = data;
                makeCallForRecipe();
            }
        } else {
            Toast.makeText(this, R.string.problem_data, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void makeCallForRecipe() {
        Call<ResponseRecipeDetails> detailsCall = RecipeCallGenerator.getCall(recipeID, RecipeListActivity.APP_ID, RecipeListActivity.API_KEY);
        detailsCall.enqueue(new Callback<ResponseRecipeDetails>() {
            @Override
            public void onResponse(Call<ResponseRecipeDetails> call, Response<ResponseRecipeDetails> response) {
                if (response.isSuccessful()) {
                    recipeDetails = response.body();
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), RecipeDetailsActivity.this, prepareBundle());
                    setUpViewPager();
                }
            }

            @Override
            public void onFailure(Call<ResponseRecipeDetails> call, Throwable t) {
                Toast.makeText(RecipeDetailsActivity.this, R.string.problem_data, Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "Response failed: " + t.getMessage());
            }
        });
    }

    private void setUpViewPager() {
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private Bundle prepareBundle() {
        String name = recipeDetails.getName();
        int time = recipeDetails.getTotalTimeInSeconds();
        List<String> ingredients = recipeDetails.getIngredientLines();
        Log.v(LOG_TAG, "ingredients size: " + ingredients.size());
        String portions = recipeDetails.getYieldPortions();
        Flavors flavors = recipeDetails.getFlavors();
        String sourceURL = recipeDetails.getSource().getSourceRecipeUrl();
        String largeImageURL = recipeDetails.getImagesResources().get(0).getHostedLargeUrl();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_RECIPE_NAME, name);
        bundle.putInt(KEY_RECIPE_TIME_SECONDS, time);
        bundle.putStringArray(KEY_INGREDIENTS, ingredients.toArray(new String[ingredients.size()]));
        bundle.putString(KEY_PORTIONS, portions);
        bundle.putString(KEY_SOURCE_URL, sourceURL);
        bundle.putString(KEY_IMAGE_URL, largeImageURL);
        bundle.putParcelable(KEY_FLAVORS, flavors);
        bundle.putBundle(AboutFragment.MATCH_RECIPE_BUNDLE, bundleStart);
        return bundle;
    }
}
