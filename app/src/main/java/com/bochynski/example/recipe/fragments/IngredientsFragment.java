package com.bochynski.example.recipe.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.utils.SharedKeys;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Karlo on 2017-08-27.
 */

public class IngredientsFragment extends Fragment {

    private String[] ingredients;

    @BindView(R.id.main_frame)
    LinearLayout mainLayout;

    @BindView(R.id.no_ingredients)
    TextView noIngredients;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.ingredients_fragment, container, false);
        ButterKnife.bind(this, inflated);
        handleBundle(getArguments());
        if (ingredients != null && ingredients.length > 0) {
            generateIngredientsViews();
        } else {
            noIngredientsToDisplay();
        }
        return inflated;
    }

    private void handleBundle(Bundle bundle) {
        if (bundle != null) {
            ingredients = bundle.getStringArray(SharedKeys.KEY_INGREDIENTS);
        }
    }

    private void generateIngredientsViews() {
        noIngredients.setVisibility(View.GONE);
        Log.v("INGREDIENTS FRAGMENT", "ingredients size" + ingredients.length);
        for(int i = 0; i < ingredients.length; i++) {
            String ingredientLine = ingredients[i];
            View ingredientItem = View.inflate(getContext(), R.layout.ingredient_item, null);
            ((TextView) ingredientItem.findViewById(R.id.ingredient)).setText(ingredientLine);
            mainLayout.addView(ingredientItem);
        }
    }

    private void noIngredientsToDisplay() {
        noIngredients.setVisibility(View.VISIBLE);
    }
}
