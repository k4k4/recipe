package com.bochynski.example.recipe.view_adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.activities.RecipeDetailsActivity;
import com.bochynski.example.recipe.fragments.AboutFragment;
import com.bochynski.example.recipe.fragments.IngredientsFragment;
import com.bochynski.example.recipe.fragments.WebViewFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karlo on 2017-08-24.
 */

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> fragments;
    private String[] pageTitles;
    private Context context;
    private Bundle data;

    public SectionsPagerAdapter(FragmentManager fm, Context context, Bundle data) {
        super(fm);
        setUpFragmentList();
        this.data = data;
        this.pageTitles = context.getResources().getStringArray(R.array.page_titles);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Fragment fragment = Fragment.instantiate(context, fragments.get(position));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }

    private void setUpFragmentList() {
        fragments = new ArrayList<>();
        fragments.add(AboutFragment.class.getName());
        fragments.add(IngredientsFragment.class.getName());
        fragments.add(WebViewFragment.class.getName());
    }
}
