
package com.bochynski.example.recipe.model.response_brief;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageUrlsBySize implements Serializable {

    @SerializedName("90")
    @Expose
    private String imageURL_90px;

    public String getImageURL_90px() {
        return imageURL_90px;
    }

    public void setImageURL_90px(String imageURL_90px) {
        this.imageURL_90px = imageURL_90px;
    }

}
