package com.bochynski.example.recipe.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.utils.SharedKeys;

/**
 * Created by Karlo on 2017-08-27.
 */

public class WebViewFragment extends Fragment {

    private WebView webView;
    private String sourceURL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.web_view_fragment, container, false);
        webView = (WebView) inflated.findViewById(R.id.web_view);
        handleBundle(getArguments());
        webView.loadUrl(sourceURL);
        return inflated;
    }

    private void handleBundle(Bundle bundle) {
        if (bundle != null) {
            sourceURL = bundle.getString(SharedKeys.KEY_SOURCE_URL);
        }
    }
}
