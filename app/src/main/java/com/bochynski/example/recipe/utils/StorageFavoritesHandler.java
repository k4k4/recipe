package com.bochynski.example.recipe.utils;

import android.content.Context;

import com.bochynski.example.recipe.model.response_brief.MatchRecipe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Karlo on 2017-08-27.
 */

public class StorageFavoritesHandler {

    private static final String FILE_NAME = "favorites";

    private static boolean saveFavorites(List<MatchRecipe> recipes, Context context) {
        boolean successful = true;
        try {
            FileOutputStream outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectStream = new ObjectOutputStream(outputStream);
            objectStream.writeObject(recipes);
            outputStream.close();
            objectStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            successful = false;
        } catch (IOException e) {
            e.printStackTrace();
            successful = false;
        }
        return successful;
    }

    public static boolean addToFavorites(MatchRecipe recipe, Context context) {
        if (recipe == null) {
            return false;
        }
        List<MatchRecipe> favorites = getFavorites(context);
        if (favorites == null) {
            favorites = new LinkedList<>();
        }
        favorites.add(recipe);
        boolean successful = true;
        try {
            FileOutputStream outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectStream = new ObjectOutputStream(outputStream);
            objectStream.writeObject(favorites);
            outputStream.close();
            objectStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            successful = false;
        } catch (IOException e) {
            e.printStackTrace();
            successful = false;
        }
        return successful;
    }

    public static List<MatchRecipe> getFavorites(Context context) {
        List<MatchRecipe> recipes = null;
        try {
            FileInputStream inputStream = context.openFileInput(FILE_NAME);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            recipes = (List<MatchRecipe>) objectInputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    public static boolean removeFromFavorites(MatchRecipe recipe, Context context) {
        List<MatchRecipe> favorites = getFavorites(context);
        boolean found = false;
        for (int i = 0; i < favorites.size() && !found; i++) {
            MatchRecipe match = favorites.get(i);
            if (match.getId().equals(recipe.getId())) {
                favorites.remove(match);
                found = true;
            }
        }
        return saveFavorites(favorites, context);
    }

}
