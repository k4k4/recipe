package com.bochynski.example.recipe.api_connection;

import com.bochynski.example.recipe.model.meta_data.MetaDataBasic;
import com.bochynski.example.recipe.model.meta_data.MetaDataCourse;
import com.bochynski.example.recipe.model.meta_data.MetaDataDietAllergy;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Karlo on 2017-08-25.
 */

public class RecipeMetaDataCallGenerator {

    private static RecipeMetaDataService metaDataService;

    static {
        metaDataService = RecipeMetaDataRetroClient.getMetaDataService();
    }

    public static Call<List<MetaDataCourse>> getCallCourses(String appID, String apiKey) {
        return metaDataService.getMetadataCourse(appID, apiKey);
    }

    public static Call<List<MetaDataDietAllergy>> getCallAllergies(String appID, String apiKey) {
        return metaDataService.getMetadataAllergy(appID, apiKey);
    }

    public static Call<List<MetaDataDietAllergy>> getCallDiets(String appID, String apiKey) {
        return metaDataService.getMetadataDiet(appID, apiKey);
    }
}
