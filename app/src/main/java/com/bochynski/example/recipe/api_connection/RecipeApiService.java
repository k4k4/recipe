package com.bochynski.example.recipe.api_connection;

import com.bochynski.example.recipe.model.response_brief.ResponseBrief;
import com.bochynski.example.recipe.model.response_recipe_detailed.ResponseRecipeDetails;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Karlo on 2017-08-24.
 */

public interface RecipeApiService {

    /*
     * calling recipes
     */
    @GET("recipes?requirePictures=true")
    Call<ResponseBrief> getRecipes(@Query("_app_id") String appID,
                                   @Query("_app_key") String apiKey,
                                   @Query("q") String search,
                                   @Query("maxTotalTimeInSeconds") String maxTimeSeconds,
                                   @Query("allowedDiet[]") String[] dietsAllowed,
                                   @Query("allowedAllergy[]") String[] allergiesAllowed,
                                   @Query("allowedCourse[]") String allowedCourse);

    /*
     * calling recipe by its id for detailed info
     */
    @GET("recipe/{recipeID}")
    Call<ResponseRecipeDetails> getRecipeInfo(@Path("recipeID") String recipeID, @Query("_app_id") String appID, @Query("_app_key") String apiKey);
}
