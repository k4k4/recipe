package com.bochynski.example.recipe.utils;

/**
 * Created by Karlo on 2017-08-27.
 */

public class SharedKeys {

    public static final String KEY_BUNDLE_MATCH_RECIPE = "match recipe";
    public static final String KEY_RECIPE_NAME = "recipe name";
    public static final String KEY_RECIPE_TIME_SECONDS = "recipe time seconds";
    public static final String KEY_INGREDIENTS = "ingredients";
    public static final String KEY_PORTIONS = "portions";
    public static final String KEY_FLAVORS = "flavors";
    public static final String KEY_SOURCE_URL = "source url";
    public static final String KEY_IMAGE_URL = "image url";

    public static final String KEY_IMAGE_MR = "image mr";
    public static final String KEY_SCORE_MR = "score mr";
    public static final String KEY_TITLE = "title mr";
    public static final String KEY_PREPTIME = "prep time mr";
    public static final String KEY_ID = "id mr";

    private SharedKeys() {

    }
}
