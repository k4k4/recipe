package com.bochynski.example.recipe.view_adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.activities.RecipeDetailsActivity;
import com.bochynski.example.recipe.activities.RecipeListActivity;
import com.bochynski.example.recipe.fragments.RecipeDetailsFragment;
import com.bochynski.example.recipe.model.response_brief.MatchRecipe;

import java.util.List;

import static com.bochynski.example.recipe.utils.SharedKeys.*;

/**
 * Created by Karlo on 2017-08-25.
 */

public class RecipeItemRecyclerViewAdapter extends RecyclerView.Adapter<RecipeItemViewHolder> {

    private List<MatchRecipe> recipeList;
    private boolean isTwoPane;
    private Context context;

    public RecipeItemRecyclerViewAdapter(Context context, boolean isTwoPane, List<MatchRecipe> recipes) {
        recipeList = recipes;
        this.isTwoPane = isTwoPane;
        this.context = context;
    }

    @Override
    public RecipeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_list_item, parent, false);
        return new RecipeItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipeItemViewHolder holder, int position) {
        final MatchRecipe matchRecipe = recipeList.get(position);
        holder.setRecipeImage(context, matchRecipe.getImageUrlsBySize().getImageURL_90px());
        holder.setRecipeScore(context, matchRecipe.getRating());
        holder.setRecipeTitle(matchRecipe.getRecipeName());
        holder.setRecipePreparationTime(context, matchRecipe.getTotalTimeInSeconds());
        holder.getViewFrame().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putBundle(KEY_BUNDLE_MATCH_RECIPE, prepareBundle(matchRecipe));
                    RecipeDetailsFragment fragment = new RecipeDetailsFragment();
                    fragment.setArguments(arguments);
                    ((RecipeListActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.recipe_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, RecipeDetailsActivity.class);
                    intent.putExtra(KEY_BUNDLE_MATCH_RECIPE, prepareBundle(matchRecipe));
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipeList == null ? 0 : recipeList.size();
    }

    private Bundle prepareBundle(MatchRecipe matchRecipe) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_IMAGE_MR, matchRecipe.getImageUrlsBySize().getImageURL_90px());
        bundle.putInt(KEY_SCORE_MR, matchRecipe.getRating());
        bundle.putString(KEY_TITLE, matchRecipe.getRecipeName());
        bundle.putInt(KEY_PREPTIME, matchRecipe.getTotalTimeInSeconds());
        bundle.putString(KEY_ID, matchRecipe.getId());
        return bundle;
    }
}
