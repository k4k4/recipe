package com.bochynski.example.recipe.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.api_connection.RecipeCallGenerator;
import com.bochynski.example.recipe.model.response_recipe_detailed.Flavors;
import com.bochynski.example.recipe.model.response_recipe_detailed.ResponseRecipeDetails;
import com.bochynski.example.recipe.view_adapters.SectionsPagerAdapter;
import com.bochynski.example.recipe.activities.RecipeDetailsActivity;
import com.bochynski.example.recipe.activities.RecipeListActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bochynski.example.recipe.fragments.AboutFragment.MATCH_RECIPE_BUNDLE;
import static com.bochynski.example.recipe.utils.SharedKeys.*;

/**
 * A fragment representing a single Recipe detail screen.
 * This fragment is either contained in a {@link RecipeListActivity}
 * in two-pane mode (on tablets) or a {@link RecipeDetailsActivity}
 * on handsets.
 */
public class RecipeDetailsFragment extends Fragment {

    public static final String SEARCH_ID_KEY = "search id";

    private static final String LOG_TAG = RecipeDetailsFragment.class.getSimpleName();
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String recipeID;
    private ResponseRecipeDetails recipeDetails;
    private Bundle bundleStart;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RecipeDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        Bundle data = arguments.getBundle(KEY_BUNDLE_MATCH_RECIPE);
        bundleStart = data;
        if (data != null) {
            recipeID = data.getString(KEY_ID);
        }
        callForRecipeAndInitPagerIfSuccessful();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recipe_details_fragment, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.container);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        return rootView;
    }

    private void callForRecipeAndInitPagerIfSuccessful() {
        Call<ResponseRecipeDetails> detailsCall = RecipeCallGenerator.getCall(recipeID, RecipeListActivity.APP_ID, RecipeListActivity.API_KEY);
        detailsCall.enqueue(new Callback<ResponseRecipeDetails>() {
            @Override
            public void onResponse(Call<ResponseRecipeDetails> call, Response<ResponseRecipeDetails> response) {
                if (response.isSuccessful()) {
                    recipeDetails = response.body();
                    sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), getContext(), prepareBundle());
                    setUpViewPager();
                }
            }

            @Override
            public void onFailure(Call<ResponseRecipeDetails> call, Throwable t) {
                Toast.makeText(getContext(), R.string.problem_data, Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "Response failed: " + t.getMessage());
            }
        });
    }

    private void setUpViewPager() {
        // Set up the ViewPager with the sections adapter.
        viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private Bundle prepareBundle() {
        String name = recipeDetails.getName();
        int time = recipeDetails.getTotalTimeInSeconds();
        List<String> ingredients = recipeDetails.getIngredientLines();
        String portions = recipeDetails.getYieldPortions();
        Flavors flavors = recipeDetails.getFlavors();
        String sourceURL = recipeDetails.getSource().getSourceRecipeUrl();
        String largeImageURL = recipeDetails.getImagesResources().get(0).getHostedLargeUrl();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_RECIPE_NAME, name);
        bundle.putInt(KEY_RECIPE_TIME_SECONDS, time);
        bundle.putStringArray(KEY_INGREDIENTS, ingredients.toArray(new String[ingredients.size()]));
        bundle.putString(KEY_PORTIONS, portions);
        bundle.putString(KEY_SOURCE_URL, sourceURL);
        bundle.putString(KEY_IMAGE_URL, largeImageURL);
        bundle.putParcelable(KEY_FLAVORS, flavors);
        bundle.putBundle(AboutFragment.MATCH_RECIPE_BUNDLE, bundleStart);
        return bundle;
    }

}
