package com.bochynski.example.recipe.views_utils;

/**
 * Created by Karlo on 2017-08-25.
 */

public class NavigationItem {

    private String text;
    private int iconResource;
    private String searchValue;

    public NavigationItem(String text, int iconResource, String searchValue) {
        this.text = text;
        this.iconResource = iconResource;
        this.searchValue = searchValue;
    }

    public String getText() {
        return text;
    }

    public int getIconResource() {
        return iconResource;
    }

    public String getSearchValue() {
        return searchValue;
    }
}
