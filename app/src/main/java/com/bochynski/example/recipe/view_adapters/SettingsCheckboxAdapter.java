package com.bochynski.example.recipe.view_adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.model.meta_data.MetaDataDietAllergy;

import java.util.List;

/**
 * Created by Karlo on 2017-08-26.
 */

public class SettingsCheckboxAdapter extends ArrayAdapter<MetaDataDietAllergy> {

    private Context context;
    private int viewResource;
    private List<MetaDataDietAllergy> items;
    private CheckBox checkBox;
    private List<String> userSelection;

    public SettingsCheckboxAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<MetaDataDietAllergy> objects, List<String> userSelection) {
        super(context, resource, objects);
        this.context = context;
        this.viewResource = resource;
        this.items = objects;
        this.userSelection = userSelection;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View viewItem = convertView;
        if (viewItem == null) {
            viewItem = LayoutInflater.from(context).inflate(viewResource, parent, false);
            setUpView(position, viewItem);
        } else {
            setUpView(position, viewItem);
        }
        return viewItem;
    }

    private void setUpView(final int position, View view) {
        checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        MetaDataDietAllergy item = items.get(position);
        checkBox.setText(item.getShortDescription());
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchValue = items.get(position).getSearchValue();
                if (userSelection.contains(searchValue)) {
                    userSelection.remove(searchValue);
                } else {
                    userSelection.add(searchValue);
                }
                Log.v("Checkbox log", "userSelection size: " + userSelection.size());
            }
        });
    }
}
