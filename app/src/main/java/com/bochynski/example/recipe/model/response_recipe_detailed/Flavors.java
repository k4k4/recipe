package com.bochynski.example.recipe.model.response_recipe_detailed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Flavors implements Parcelable, Serializable {

    @SerializedName("Piquant")
    @Expose
    private Double piquant;
    @SerializedName("Meaty")
    @Expose
    private Double meaty;
    @SerializedName("Bitter")
    @Expose
    private Double bitter;
    @SerializedName("Sweet")
    @Expose
    private Double sweet;
    @SerializedName("Sour")
    @Expose
    private Double sour;
    @SerializedName("Salty")
    @Expose
    private Double salty;

    public Double getPiquant() {
        return piquant;
    }

    public void setPiquant(Double piquant) {
        this.piquant = piquant;
    }

    public Double getMeaty() {
        return meaty;
    }

    public void setMeaty(Double meaty) {
        this.meaty = meaty;
    }

    public Double getBitter() {
        return bitter;
    }

    public void setBitter(Double bitter) {
        this.bitter = bitter;
    }

    public Double getSweet() {
        return sweet;
    }

    public void setSweet(Double sweet) {
        this.sweet = sweet;
    }

    public Double getSour() {
        return sour;
    }

    public void setSour(Double sour) {
        this.sour = sour;
    }

    public Double getSalty() {
        return salty;
    }

    public void setSalty(Double salty) {
        this.salty = salty;
    }


    protected Flavors(Parcel in) {
        piquant = in.readByte() == 0x00 ? null : in.readDouble();
        meaty = in.readByte() == 0x00 ? null : in.readDouble();
        bitter = in.readByte() == 0x00 ? null : in.readDouble();
        sweet = in.readByte() == 0x00 ? null : in.readDouble();
        sour = in.readByte() == 0x00 ? null : in.readDouble();
        salty = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (piquant == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(piquant);
        }
        if (meaty == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(meaty);
        }
        if (bitter == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(bitter);
        }
        if (sweet == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(sweet);
        }
        if (sour == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(sour);
        }
        if (salty == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(salty);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Flavors> CREATOR = new Parcelable.Creator<Flavors>() {
        @Override
        public Flavors createFromParcel(Parcel in) {
            return new Flavors(in);
        }

        @Override
        public Flavors[] newArray(int size) {
            return new Flavors[size];
        }
    };
}