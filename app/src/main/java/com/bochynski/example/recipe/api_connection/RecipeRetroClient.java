package com.bochynski.example.recipe.api_connection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Karlo on 2017-08-24.
 */

public class RecipeRetroClient {

    private static final String BASE_URL = "http://api.yummly.com/v1/api/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static RecipeApiService getApiService() {
        return getRetrofitInstance().create(RecipeApiService.class);
    }
}
