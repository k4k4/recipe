package com.bochynski.example.recipe.view_adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.views_utils.NavigationItem;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Karlo on 2017-08-25.
 */

public class NavigationItemArrayAdapter extends ArrayAdapter<NavigationItem> {

    private List<NavigationItem> items;
    private Context context;
    private int viewResource;
    private ImageView navIcon;
    private TextView navText;

    public NavigationItemArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<NavigationItem> objects) {
        super(context, resource, objects);
        this.items = objects;
        this.viewResource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View navigationView = convertView;
        if (navigationView == null) {
            navigationView = LayoutInflater.from(context).inflate(viewResource, parent, false);
            setUpView(position, navigationView);
        } else {
            setUpView(position, navigationView);
        }

        return navigationView;
    }

    private void setUpView(int position, View view) {
        navIcon = (ImageView) view.findViewById(R.id.navigation_icon);
        navText = (TextView) view.findViewById(R.id.navigation_text);
        NavigationItem item = items.get(position);
        Picasso.with(context).load(item.getIconResource()).centerInside().fit().into(navIcon);
        navText.setText(item.getText());
    }

}
