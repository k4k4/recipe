package com.bochynski.example.recipe.api_connection;

import com.bochynski.example.recipe.api_connection.RecipeApiService;
import com.bochynski.example.recipe.gsonp_utils.GsonPConverterFactory;
import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Karlo on 2017-08-25.
 */

public class RecipeMetaDataRetroClient {

    private static final String BASE_URL = "http://api.yummly.com/v1/api/metadata/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(new GsonPConverterFactory(new Gson())).build();
    }

    public static RecipeMetaDataService getMetaDataService() {
        return getRetrofitInstance().create(RecipeMetaDataService.class);
    }

}
