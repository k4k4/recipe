package com.bochynski.example.recipe.api_connection;

import com.bochynski.example.recipe.model.meta_data.MetaDataBasic;
import com.bochynski.example.recipe.model.meta_data.MetaDataCourse;
import com.bochynski.example.recipe.model.meta_data.MetaDataDietAllergy;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Karlo on 2017-08-25.
 */

public interface RecipeMetaDataService {

    @GET("course")
    Call<List<MetaDataCourse>> getMetadataCourse(@Query("_app_id") String appID, @Query("_app_key") String apiKey);

    @GET("allergy")
    Call<List<MetaDataDietAllergy>> getMetadataAllergy(@Query("_app_id") String appID, @Query("_app_key") String apiKey);

    @GET("diet")
    Call<List<MetaDataDietAllergy>> getMetadataDiet(@Query("_app_id") String appID, @Query("_app_key") String apiKey);

}
