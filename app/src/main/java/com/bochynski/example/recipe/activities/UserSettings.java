package com.bochynski.example.recipe.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.api_connection.RecipeMetaDataCallGenerator;
import com.bochynski.example.recipe.model.meta_data.MetaDataDietAllergy;
import com.bochynski.example.recipe.view_adapters.SettingsCheckboxAdapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSettings extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    //TODO handling connection drops

    public static final int RESULT_OK = 101;
    public static final int RESULT_EMPTY = 401;

    private static final String LOG_TAG = UserSettings.class.getSimpleName();

    private List<MetaDataDietAllergy> allergiesMetaData, dietsMetaData;
    private SettingsCheckboxAdapter adapterAllergies, adapterDiets;
    private List<String> userAllergies, userDiets;
    private int currentProgress;
    private int valueMinutes;

    @BindView(R.id.main_container)
    LinearLayout mainContainer;

    @BindView(R.id.allergies_container)
    LinearLayout allergiesLayout;

    @BindView(R.id.diets_container)
    LinearLayout dietsLayout;

    @BindView(R.id.settings_seek_bar)
    SeekBar maxTimeBar;

    @BindView(R.id.seek_bar_progress)
    TextView seekBarProgress;

    @BindView(R.id.data_views)
    LinearLayout dataHolder;

    @BindView(R.id.loading_info_views)
    LinearLayout loadingViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        ButterKnife.bind(this);
        currentProgress = 0;
        valueMinutes = 0;
        userAllergies = new LinkedList<>();
        userDiets = new LinkedList<>();
        maxTimeBar.setOnSeekBarChangeListener(this);
        setUpLoadingView();
        makeMetaDataCall();
    }

    private void makeMetaDataCall() {
        allergiesMetaData = new ArrayList<>();
        dietsMetaData = new ArrayList<>();
        Call<List<MetaDataDietAllergy>> callAllergies = RecipeMetaDataCallGenerator.getCallAllergies(RecipeListActivity.APP_ID, RecipeListActivity.API_KEY);
        Call<List<MetaDataDietAllergy>> callDiets = RecipeMetaDataCallGenerator.getCallDiets(RecipeListActivity.APP_ID, RecipeListActivity.API_KEY);

        callAllergies.enqueue(new MyCallback(allergiesMetaData, adapterAllergies, allergiesLayout, userAllergies, false));
        callDiets.enqueue(new MyCallback(dietsMetaData, adapterDiets, dietsLayout, userDiets, true));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        currentProgress = progress;
        valueMinutes = currentProgress * 5;
        setProgressValueView();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();
        if (itemID == android.R.id.home) {
            prepareIntentAndLaunch();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        prepareIntentAndLaunch();
    }

    private void prepareIntentAndLaunch() {
        Intent intent = new Intent();
        intent.putExtra(RecipeListActivity.SEARCH_KEY_ALLERGIES, userAllergies.toArray(new String[userAllergies.size()]));
        intent.putExtra(RecipeListActivity.SEARCH_KEY_DIETS, userDiets.toArray(new String[userDiets.size()]));
        if (valueMinutes == 0) {
            valueMinutes = Integer.MAX_VALUE;
        }
        intent.putExtra(RecipeListActivity.SEARCH_KEY_MAX_TIME, valueMinutes);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void setProgressValueView() {
        int hours = valueMinutes / 60;
        int minutes = valueMinutes % 60;
        String result;
        if (valueMinutes > 1440 || valueMinutes == 0) { // user wants any time
            result = getResources().getString(R.string.any_time);
            valueMinutes = Integer.MAX_VALUE;
        } else if (hours > 0) {
            result = String.format("%dh %dmin", hours, minutes);
        } else {
            result = String.format("%dmin", minutes);
        }
        seekBarProgress.setText(result);
    }

    public class MyCallback implements Callback<List<MetaDataDietAllergy>> {

        private List<MetaDataDietAllergy> metaData;
        private SettingsCheckboxAdapter adapter;
        private LinearLayout layout;
        private List<String> userSelection;
        private boolean isLastCall;

        public MyCallback(List<MetaDataDietAllergy> toInit, SettingsCheckboxAdapter adapter, LinearLayout layout, List<String> userSelection, boolean isLastCall) {
            metaData = toInit;
            this.adapter = adapter;
            this.layout = layout;
            this.userSelection = userSelection;
            this.isLastCall = isLastCall;
        }

        @Override
        public void onResponse(Call<List<MetaDataDietAllergy>> call, Response<List<MetaDataDietAllergy>> response) {
            if (response.isSuccessful()) {
                Log.v(LOG_TAG, "Response successful");
                Log.v(LOG_TAG, "" + call.request().url().toString());
                for (MetaDataDietAllergy item : response.body()) {
                    metaData.add(item);
                }
                adapter = new SettingsCheckboxAdapter(UserSettings.this, R.layout.checkbox_item, metaData, userSelection);
                for (int i = 0; i < metaData.size(); i++) {
                    layout.addView(adapter.getView(i, null, null));
                }
                if (isLastCall) {
                    setUpDataViews();
                }
            }
        }

        @Override
        public void onFailure(Call<List<MetaDataDietAllergy>> call, Throwable t) {
            Log.e(LOG_TAG, "Meta data call failure " + t.getMessage());
        }
    }

    private void setUpLoadingView() {
        loadingViews.setVisibility(View.VISIBLE);
        dataHolder.setVisibility(View.GONE);
        ScrollView.LayoutParams params = new ScrollView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        mainContainer.setLayoutParams(params);
    }

    private void setUpDataViews() {
        loadingViews.setVisibility(View.GONE);
        dataHolder.setVisibility(View.VISIBLE);
        ScrollView.LayoutParams params = new ScrollView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.NO_GRAVITY;
        mainContainer.setLayoutParams(params);
    }

}
