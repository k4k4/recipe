package com.bochynski.example.recipe.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.bochynski.example.recipe.R;
import com.bochynski.example.recipe.model.response_brief.ImageUrlsBySize;
import com.bochynski.example.recipe.model.response_brief.MatchRecipe;
import com.bochynski.example.recipe.model.response_recipe_detailed.Flavors;
import com.bochynski.example.recipe.utils.SharedKeys;
import com.bochynski.example.recipe.utils.StorageFavoritesHandler;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.bochynski.example.recipe.utils.SharedKeys.KEY_ID;
import static com.bochynski.example.recipe.utils.SharedKeys.KEY_IMAGE_MR;
import static com.bochynski.example.recipe.utils.SharedKeys.KEY_PREPTIME;
import static com.bochynski.example.recipe.utils.SharedKeys.KEY_SCORE_MR;
import static com.bochynski.example.recipe.utils.SharedKeys.KEY_TITLE;

/**
 * Created by Karlo on 2017-08-27.
 */

public class AboutFragment extends Fragment {

    private static final int BAR_MAX = 1;
    public static final String MATCH_RECIPE_BUNDLE = "bundle match recipe";

    @BindView(R.id.toolbar_image)
    ImageView imageToolbar;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    /**
     * notice that order is crucial for proper tastes display, {@link #setUpFlavorBars()}
     */
    @BindViews({R.id.taste_sweet, R.id.taste_meat, R.id.taste_bitter, R.id.taste_salty, R.id.taste_spicy, R.id.taste_sour})
    List<IconRoundCornerProgressBar> tastesBars;

    @BindView(R.id.preparation_time)
    TextView timeTextView;

    @BindView(R.id.portions)
    TextView portionsTextView;

    @BindView(R.id.tastes)
    LinearLayout tastesLayout;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    private String title;
    private String imageURL;
    private int preparationTime;
    private String portions;
    private Flavors flavors;
    private MatchRecipe matchRecipe;
    private boolean isInFavorites;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.recipe_about_tab, container, false);
        ButterKnife.bind(this, inflated);
        handleBundle(getArguments());
        toolbarLayout.setTitle(title);
        for (IconRoundCornerProgressBar bar : tastesBars) {
            bar.setMax(BAR_MAX);
        }
        setUpImageView();
        setUpTime();
        setUpPortions();
        setUpFlavorBars();
        setFloatingActionButton();
        return inflated;
    }

    private void handleBundle(Bundle bundle) {
        if (bundle != null) {
            imageURL = bundle.getString(SharedKeys.KEY_IMAGE_URL);
            preparationTime = bundle.getInt(SharedKeys.KEY_RECIPE_TIME_SECONDS);
            portions = bundle.getString(SharedKeys.KEY_PORTIONS);
            flavors = bundle.getParcelable(SharedKeys.KEY_FLAVORS);
            title = bundle.getString(SharedKeys.KEY_RECIPE_NAME);
            getMatchRecipeItem(bundle.getBundle(MATCH_RECIPE_BUNDLE));
        }
    }

    private void setUpImageView() {
        Picasso.with(getContext()).load(imageURL).centerCrop().fit().into(imageToolbar);
    }

    private void setUpTime() {
        int hours = preparationTime / 3600;
        int minutes = (preparationTime % 3600) / 60;
        if (hours > 0) {
            timeTextView.setText(hours + "h " + minutes + "min");
        } else {
            timeTextView.setText(minutes + "min");
        }
    }

    private void setUpPortions() {
        portionsTextView.setText(portions);
    }

    private void setUpFlavorBars() {
        if (flavors != null && flavors.getSweet() != null) {
            Double sweet = flavors.getSweet();
            Double meat = flavors.getMeaty();
            Double bitter = flavors.getBitter();
            Double salty = flavors.getSalty();
            Double spicy = flavors.getPiquant();
            Double sour = flavors.getSour();
            tastesBars.get(0).setProgress(sweet.floatValue());
            tastesBars.get(1).setProgress(meat.floatValue());
            tastesBars.get(2).setProgress(bitter.floatValue());
            tastesBars.get(3).setProgress(salty.floatValue());
            tastesBars.get(4).setProgress(spicy.floatValue());
            tastesBars.get(5).setProgress(sour.floatValue());
            tastesLayout.setVisibility(View.VISIBLE);
        } else {
            tastesLayout.setVisibility(View.GONE);
        }
    }

    private void getMatchRecipeItem(Bundle data) {
        String image90pxUrl = data.getString(KEY_IMAGE_MR);
        String name = data.getString(KEY_TITLE);
        String id = data.getString(KEY_ID);
        int prepTimeSeconds = data.getInt(KEY_PREPTIME);
        int score = data.getInt(KEY_SCORE_MR);
        matchRecipe = new MatchRecipe();
        matchRecipe.setId(id);
        matchRecipe.setRecipeName(name);
        matchRecipe.setRating(score);
        matchRecipe.setTotalTimeInSeconds(prepTimeSeconds);
        ImageUrlsBySize imageUrlsBySize = new ImageUrlsBySize();
        imageUrlsBySize.setImageURL_90px(image90pxUrl);
        matchRecipe.setImageUrlsBySize(imageUrlsBySize);
    }

    private void setFloatingActionButton() {
        List<MatchRecipe> recipes = StorageFavoritesHandler.getFavorites(getContext());
        isInFavorites = false;
        if (recipes != null) {
            for (int i = 0; i < recipes.size() && !isInFavorites; i++) {
                MatchRecipe match = recipes.get(i);
                if (match.getId().equals(matchRecipe.getId())) {
                    isInFavorites = true;
                }
            }
        }
        if (isInFavorites) {
            fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_red_24dp));
        } else {
            fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_white_24dp));
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInFavorites) {
                    StorageFavoritesHandler.removeFromFavorites(matchRecipe, getContext());
                    fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_white_24dp));
                } else {
                    StorageFavoritesHandler.addToFavorites(matchRecipe, getContext());
                    fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_red_24dp));
                }
            }
        });
    }
}
