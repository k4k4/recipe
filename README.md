Recipe app

Search your favorites recipes. User can browse recipes by category, add additional key words, diets/ allergies to satisfy and maximum meal preparation time.
Add/ remove from favorites.

API used: https://developer.yummly.com/documentation

Project uses:
Saving favorites made with internal storage. Recipe source with WebView.
API calls made with Retrofit
Butterknife and Picasso

### Recipe details
![image](shoots/details_about.png)

### Allergies / diets
![image](shoots/diets_allergies_time.png)

### Favorites
![image](shoots/favorites.png)

### Ingredients
![image](shoots/ingredients.png)

### Nav bar
![image](shoots/navigation.png)

### Query keywords
![image](shoots/query.png)

### Recipe source nested in WebView
![image](shoots/sourceWebView.png)

### Tablet mode, master / detail flow
![image](shoots/tablet_mode.png)

To run:
Android Studio and/or some Android device with API 19+.
All dependencies are included in gradle files.

Feel free to point out any mistakes or bad habits.
If using code for more than learning it would be nice if you leave me credit and inform about that.